//
//  Company.swift
//  DaPulse
//
//  Created by Mr. Cohen on 18/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes


struct Company {
	let company_name: String
	let employees: [Employee]
}

extension Company: Decodable {
	public static func decode(_ json: JSON) -> Decoded<Company> {
		return curry(Company.init)
			<^> json <| "company_name"
			<*> json <|| "employees"
	}
}
