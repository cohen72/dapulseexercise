//
//  RootViewController.swift
//  DaPulse
//
//  Created by Mr. Cohen on 18/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

	@IBOutlet weak var splashView: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		CompanyManager.shared.getCompany { success in
			self.performSegue(withIdentifier: "showHighlevel", sender: nil)
		}
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let identifier = segue.identifier {
			if identifier == "showHighlevel" {
				let nav = segue.destination as? UINavigationController
				let workerCollectionVC = nav?.viewControllers[0] as? WorkerCollectionVC
				workerCollectionVC?.isRoot = true
				workerCollectionVC?.title = "DaCompany"
			}
		}
	}

	
}

