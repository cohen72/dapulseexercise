//
//  CompanyManager.swift
//  DaPulse
//
//  Created by Mr. Cohen on 18/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import UIKit
import Argo

class CompanyManager {

	static let shared = CompanyManager()
	
	var company:Company!
	
	func getCompany(callback: @escaping (Bool) -> Void) {
		
		Services.request(target: .workersList, success: { json in
			
			let j: Decoded<Company> = decode(json)
			
			guard let company = j.value else {
				print("could not unwrap value");
				callback(false)
				return
			}
			
			self.company = company
			callback(true)
			
		}) { error in
			print(error)
			callback(false)
		}
		
	}
	
	private var highLevelManagers:[Employee] {
		get {
			let managers = self.company.employees.filter {
				$0.is_manager == true && $0.manager_id == nil
			}
			return managers
		}
	}
	
	func getManagerEmployees(manager: Employee?) -> [Employee] {
		if manager == nil {
			return highLevelManagers
		}
		let managerEmployees = self.company.employees.filter {
			$0.manager_id == manager?.id
		}
		return managerEmployees
	}
	
		
}
