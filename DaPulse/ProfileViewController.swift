//
//  ProfileViewController.swift
//  DaPulse
//
//  Created by Mr. Cohen on 19/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import UIKit
import ConfettiView

class ProfileViewController: UIViewController {

	@IBOutlet weak var employeeView: EmployeeDesignableView!
	@IBOutlet weak var lblPhone: UILabel!
	@IBOutlet weak var lblEmail: UILabel!
	@IBOutlet weak var confettiView: ConfettiView!
	
	var employee: Employee!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.lblPhone.text = employee?.phone
		self.lblEmail.text = employee?.email
		self.employeeView.configure(employee: employee)
		self.employeeView.setImageWidth(width: 120)
		confettiView.stopAnimating()
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
	}
	
	@IBAction func doit(_ sender: Any) {
		confettiView.isAnimating ? confettiView.stopAnimating() : confettiView.startAnimating()
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
