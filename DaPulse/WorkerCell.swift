//
//  WorkerCell.swift
//  DaPulse
//
//  Created by Mr. Cohen on 18/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import UIKit

class WorkerCell: UICollectionViewCell {
	
	@IBOutlet weak var employeeView: EmployeeDesignableView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.selectedBackgroundView = UIView.init(frame: self.bounds)
		self.selectedBackgroundView?.backgroundColor = UIColor.clear
	}
	
	func configure(employee: Employee) {
		employeeView.configure(employee: employee)
	}
	
	func highlight() {
		employeeView.highlight()
	}
	
	func unhighlight() {
		employeeView.unhighlight()
	}
	
}
