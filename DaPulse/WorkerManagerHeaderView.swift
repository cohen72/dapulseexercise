//
//  WorkerManagerHeaderView.swift
//  DaPulse
//
//  Created by Mr. Cohen on 18/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import UIKit
import AlamofireImage

class WorkerManagerHeaderView: UICollectionReusableView {
	
	@IBOutlet weak var lblDepartmentName: UILabel!
	@IBOutlet weak var employeeView: EmployeeDesignableView!
	@IBOutlet weak var lblBreadCrumbs: UILabel!
	@IBOutlet weak var lblEmployees: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	func configure(manager: Employee?) {
		if let m = manager {
			self.configure(manager: m)
		} else {
			self.configure(company: CompanyManager.shared.company)
		}
	}
	
	
	private func configure(manager: Employee) {
		self.lblDepartmentName.text = manager.department
		employeeView.configure(employee: manager)
	}
	
	private func configure(company: Company) {
		employeeView.alpha = 0.0
		lblEmployees.alpha = 0.0
		self.lblBreadCrumbs.alpha = 0.0
		self.lblDepartmentName.text = company.company_name
	}
	
}
