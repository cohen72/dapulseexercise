//
//  BreweryAPI.swift
//  ColuBrew
//
//  Created by Mr. Cohen on 22/11/2016.
//  Copyright © 2016 Mr. Cohen. All rights reserved.
//

import UIKit
import Moya

public enum daPulseAPI {
    case workersList
}

extension daPulseAPI: TargetType {

    public var baseURL: URL { return URL(string: "https://dapulse-mobile-test.herokuapp.com")! }
    public var path: String {
        return ""
    }
    public var method: Moya.Method {
        return .get
    }
    public var parameters: [String: Any]? {
        return nil
    }
    public var task: Task {
        return .request
    }
    public var validate: Bool {
        return true
    }
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }
	/// The method used for parameter encoding.
	public var parameterEncoding: ParameterEncoding {
		return URLEncoding.default
	}
}

