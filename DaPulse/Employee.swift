//
//  Employee.swift
//  DaPulse
//
//  Created by Mr. Cohen on 18/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes


struct Employee {
	let id: Int
	let name: String
	let phone: String
	let email: String
	let title: String
	let profile_pic: String
	let department: String
	let is_manager: Bool?
	let manager_id: Int?
	
}

extension Employee: Decodable {
	public static func decode(_ json: JSON) -> Decoded<Employee> {
		let e = curry(Employee.init)
		return e
			<^> json <| "id"
			<*> json <| "name"
			<*> json <| "phone"
			<*> json <| "email"
			<*> json <| "title"
			<*> json <| "profile_pic"
			<*> json <| "department"
			<*> json <|? "is_manager"
			<*> json <|? "manager_id"
	}
}
