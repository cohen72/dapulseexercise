
import Foundation

import UIKit


enum TransitionStatus {
	case present
	case dismiss
}

class ViewControllerTransition: NSObject, UIViewControllerAnimatedTransitioning  {

	var transitionStatus: TransitionStatus

	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		return 0.4
	}

	init(status: TransitionStatus = .present) {
		transitionStatus = status
		super.init()
	}

	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
		let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
		let containView = transitionContext.containerView

		if transitionStatus == .dismiss {
			containView.addSubview(toVC!.view)
			containView.addSubview(fromVC!.view)
			toVC!.view.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
		} else if transitionStatus == .present {
			toVC!.view.alpha = 0.0
			toVC!.view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
			containView.addSubview(toVC!.view)
		}

		UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
			if self.transitionStatus == .dismiss {
				fromVC!.view.alpha = 0.0
				fromVC!.view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
			} else if self.transitionStatus == .present {
				fromVC!.view.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
				toVC!.view.alpha = 1.0
			}
			toVC!.view.transform = CGAffineTransform.identity
			
		}) { finished in
			transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
		}
	}

}
