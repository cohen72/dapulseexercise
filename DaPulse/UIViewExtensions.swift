//
//  UIViewExtensions.swift
//  DaPulse
//
//  Created by Mr. Cohen on 19/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import UIKit

public extension UIView {
	
	public func circularize(borderWidth: CGFloat, borderColor: UIColor?) {
		self.layer.cornerRadius = self.layer.bounds.width / 2
		self.layer.borderWidth = borderWidth
		self.layer.masksToBounds = true
		if let color = borderColor {
			self.layer.borderColor = color.cgColor
		}
	}
}
