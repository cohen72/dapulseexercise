//
//  EmployeeDesignableView.swift
//  DaPulse
//
//  Created by Mr. Cohen on 19/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import UIKit
import AlamofireImage

@IBDesignable

class EmployeeDesignableView: UIView {
	
	var contentView : UIView!
	
	@IBOutlet weak var imgPicture: UIImageView!
	@IBOutlet private weak var lblName: UILabel!
	@IBOutlet private weak var lblTitle: UILabel!
	@IBOutlet private weak var highlightView: UIView!
	@IBOutlet private weak var constraintImageWidth: NSLayoutConstraint!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		xibSetup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		xibSetup()
	}
	
	func xibSetup() {
		contentView = loadViewFromNib()
		
		// use bounds not frame or it'll be offset
		contentView.frame = bounds
		
		// Make the view stretch with containing view
		contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
		
		// Adding custom subview on top of our view (over any custom drawing > see note below)
		addSubview(contentView)
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		imgPicture.circularize(borderWidth: 0.5, borderColor: UIColor.lightGray)
		highlightView.circularize(borderWidth: 0.0, borderColor: nil)
	}
	
	func loadViewFromNib() -> UIView! {
		let bundle = Bundle(for: type(of: self))
		let nib = UINib(nibName: "EmployeeDesignableView", bundle: bundle)
		let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
		return view
	}
	
	func configure(employee: Employee) {
		self.lblName.text = employee.name
		self.lblTitle.text = employee.title
		let url = URL(string: employee.profile_pic)!
		self.imgPicture.af_setImage(withURL: url as URL, placeholderImage: nil, filter: nil, imageTransition: .crossDissolve(0.2))
	}
	
	func highlight() {
		UIView.animate(withDuration: 0.2) {
			self.highlightView.alpha = 1.0
		}
	}
	
	func unhighlight() {
		UIView.animate(withDuration: 0.2) {
			self.highlightView.alpha = 0.0
		}
	}
	
	func setImageWidth(width: CGFloat) {
		self.constraintImageWidth.constant = width
		self.layoutIfNeeded()
		self.imgPicture.circularize(borderWidth: 0.5, borderColor: UIColor.lightGray)
	}
}
