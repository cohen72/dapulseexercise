//
//  WorkerCollectionVC.swift
//  DaPulse
//
//  Created by Mr. Cohen on 18/01/2017.
//  Copyright © 2017 Mr. Cohen. All rights reserved.
//

import UIKit


private let reuseIdentifier = "WorkerCell"


class WorkerCollectionVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {

	var manager: Employee?
	var employees: [Employee] = []
	var isRoot: Bool = false
	var breadCrumbText: String = ""
	var sourceCell: UICollectionViewCell?

	
	lazy var cellSize: CGSize = {
		let size = CGSize.init(width: self.view.bounds.width / 3 - 0.5, height: 200)
		return size
	}()
	
	lazy var headerSize: CGSize = {
		let height: CGFloat = self.isRoot == true ? 100 : 255
		var size = CGSize.init(width: self.view.bounds.width, height: height)
		return size
	}()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		employees = CompanyManager.shared.getManagerEmployees(manager: manager)
		if self.isRoot == true {
			breadCrumbText = CompanyManager.shared.company.company_name
		}
		let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
		layout?.sectionHeadersPinToVisibleBounds = true
    }

	// MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 	employees.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! WorkerCell
		let employee = employees[indexPath.row]
		cell.configure(employee: employee)
        return cell
    }

    // MARK: UICollectionViewDelegate

	override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		switch kind {
		case UICollectionElementKindSectionHeader:
			let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "WorkerManagerHeaderView", for: indexPath) as! WorkerManagerHeaderView
			headerView.configure(manager: manager)
			headerView.lblBreadCrumbs.text = breadCrumbText
			return headerView
		default:
			assert(false, "Unexpected element kind")
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		return headerSize
	}

	override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {

		// decide which page to show next based on employee manager status
		let employee = employees[indexPath.row]
		if let isManager = employee.is_manager {
			if isManager == true {
				return true
			}
		}

		self.performSegue(withIdentifier: "showProfile", sender: indexPath)
		return false
	}
	
	override func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath) as! WorkerCell
		cell.highlight()
	}
	
	override func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath) as! WorkerCell
		cell.unhighlight()
	}
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return cellSize
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 0.5
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 0.5
	}
	

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		
		self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title:self.isRoot == true ? "Home" : self.manager?.department, style: UIBarButtonItemStyle.plain, target: nil, action: nil)
		
		if let identifier = segue.identifier {
			if identifier == "showManager" {
				self.navigationController?.delegate = self
				if let indexPaths = self.collectionView?.indexPathsForSelectedItems {
					let indexPath = indexPaths[0]
					let manager = employees[indexPath.row]
					let workerCollectionVC = segue.destination as? WorkerCollectionVC
					workerCollectionVC?.manager = manager
					workerCollectionVC?.breadCrumbText = breadCrumbText + " > " + manager.department
					sourceCell = self.collectionView?.cellForItem(at: indexPath)
				}
				
			} else if identifier == "showProfile" {
				let indexPath = sender as! IndexPath
				sourceCell = self.collectionView?.cellForItem(at: indexPath)
				let employee = employees[indexPath.row]
				let profileVc = segue.destination as? ProfileViewController
				profileVc?.employee = employee
			}
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
}



//MARK: UINavigationControllerDelegate

extension WorkerCollectionVC: UINavigationControllerDelegate {

	func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		if operation == .pop {
			return ViewControllerTransition.init(status: .dismiss)
		} else {
			return ViewControllerTransition.init(status: .present)
		}
	}
}
